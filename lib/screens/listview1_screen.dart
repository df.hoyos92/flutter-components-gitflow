import 'package:flutter/material.dart';

class ListviewScreen extends StatelessWidget {
  final options = const [
    'Megaman',
    'Meteal Gear',
    'Super Mash ',
    'Final fantas'
  ];

  const ListviewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('ListView Tipo 1'),
        ),
        body: ListView(
          children: [
            ...options
                .map((e) => ListTile(
                      title: Text(e),
                      trailing: Icon(Icons.arrow_forward_ios_outlined),
                    ))
                .toList(), //...  extraer cada elementos de la lista

            // ListTile(
            //leading: Icon(Icons.access_time_filled_sharp),
            //title: Text('hola mundo'),
            //)
          ],
        ));
  }
}
